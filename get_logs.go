package evm

import (
	"context"
	"fmt"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

type GetLogsOpts struct {
	Client          *ethclient.Client
	TransactionHash common.Hash
}

func GetLogs(opts GetLogsOpts) (Logs, error) {
	transaction, _, err := opts.Client.TransactionByHash(context.Background(), opts.TransactionHash)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve transaction[%s]: %s", opts.TransactionHash, err)
	}
	receipt, err := opts.Client.TransactionReceipt(context.Background(), transaction.Hash())
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve receipts for transaction[%s]: %s", opts.TransactionHash, err)
	}
	logsInstance := Logs{}
	if receipt.Logs != nil {
		for _, logEntry := range receipt.Logs {
			logsInstance = append(logsInstance, *NewLog(logEntry))
		}
	}
	return logsInstance, nil
}
