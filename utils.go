package evm

import (
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/types"
)

func NewBlock(block *types.Block) *Block {
	transactions := block.Transactions()
	transactionHashes := []string{}
	for _, transaction := range transactions {
		transactionHashes = append(transactionHashes, transaction.Hash().Hex())
	}
	return &Block{
		BaseFee:           block.BaseFee(),
		GasUsed:           block.GasUsed(),
		GasLimit:          block.GasLimit(),
		Hash:              block.Hash().Hex(),
		Number:            block.Number().Int64(),
		ParentHash:        block.ParentHash().Hex(),
		Timestamp:         time.Unix(int64(block.Time()), 0),
		TransactionHashes: transactionHashes,
		TransactionsCount: len(block.Transactions()),
		UncleHash:         block.UncleHash().Hex(),
		ValidatedBy:       block.Coinbase().Hex(),
	}
}

func NewLog(log *types.Log) *Log {
	topics := []string{}
	for _, txLogTopic := range log.Topics {
		topics = append(topics, txLogTopic.Hex())
	}
	data := ""
	if log.Data != nil {
		data = common.Bytes2Hex(log.Data)
	}
	return &Log{
		Address:         log.Address.Hex(),
		BlockNumber:     log.BlockNumber,
		Data:            data,
		Index:           log.Index,
		Topics:          topics,
		TransactionHash: log.TxHash.Hex(),
	}
}

func NewTransaction(
	transaction *types.Transaction,
	message *core.Message,
	receipt *types.Receipt,
	block *types.Block,
) *Transaction {
	data := common.Bytes2Hex(transaction.Data())
	method := ""
	if len(data) >= 8 {
		method = data[:8]
	}
	transactionInstance := Transaction{
		Cost:     transaction.Cost(),
		Data:     data,
		From:     message.From.Hex(),
		GasLimit: transaction.Gas(),
		Hash:     transaction.Hash().String(),
		Method:   method,
		Value:    transaction.Value(),
	}
	if receipt != nil {
		transactionInstance.BlockNumber = receipt.BlockNumber.Int64()
		transactionInstance.Index = receipt.TransactionIndex
	}

	if destination := transaction.To(); destination != nil {
		transactionInstance.To = destination.Hex()
	}
	if receipt != nil {
		transactionInstance.Logs = Logs{}
		for _, log := range receipt.Logs {
			topics := []string{}
			for _, txLogTopic := range log.Topics {
				topics = append(topics, txLogTopic.Hex())
			}
			data := ""
			if log.Data != nil {
				data = common.Bytes2Hex(log.Data)
			}
			transactionInstance.Logs = append(
				transactionInstance.Logs,
				Log{
					Address:     log.Address.Hex(),
					BlockNumber: log.BlockNumber,
					Data:        data,
					Topics:      topics,
				},
			)
		}
	}
	if block != nil {
		transactionInstance.Timestamp = time.Unix(int64(block.Time()), 0)
	}

	return &transactionInstance
}
