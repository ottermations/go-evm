package evm

import (
	"context"
	"fmt"
	"math/big"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
)

type NewClientEvent struct {
	Message string `json:"message"`
	Error   error  `json:"error"`
	Done    bool   `json:"done"`
}

type NewClientOpts struct {
	RpcUrl        string        `json:"rpcUrl" yaml:"rpcUrl"`
	RetryInterval time.Duration `json:"retryInterval" yaml:"retryInterval"`
	MaxRetries    int           `json:"maxRetries" yaml:"maxRetries"`
	Events        chan NewClientEvent
}

func NewClient(opts NewClientOpts) (*ethclient.Client, *big.Int, error) {
	// prepare input variables
	if opts.Events == nil {
		opts.Events = make(chan NewClientEvent, 10)
		// no-op
		go func() {
			for {
				if _, ok := <-opts.Events; !ok {
					return
				}
			}
		}()
		defer func() {
			opts.Events <- NewClientEvent{Done: true}
			close(opts.Events)
		}()
	}

	rpcUrl := opts.RpcUrl

	// set up client
	rpcMaxRetries := opts.MaxRetries
	if rpcMaxRetries <= 0 {
		rpcMaxRetries = 1
	}

	retryInterval := opts.RetryInterval
	if retryInterval <= 0 {
		retryInterval = 1 * time.Second
	}

	var clientInstance *ethclient.Client = nil
	var dialError error

	opts.Events <- NewClientEvent{
		Message: fmt.Sprintf("connecting to rpc[%s]...", rpcUrl),
	}

	for clientInstance == nil {
		clientInstance, dialError = ethclient.Dial(rpcUrl)
		if dialError != nil {
			opts.Events <- NewClientEvent{Error: fmt.Errorf("failed to dial rpc[%s]: %s", rpcUrl, dialError)}
			rpcMaxRetries--
			if rpcMaxRetries == 0 {
				return nil, nil, fmt.Errorf("failed to connect to rpc[%s] after %v tries: %s", rpcUrl, opts.MaxRetries, dialError)
			}
			opts.Events <- NewClientEvent{Message: fmt.Sprintf("trying to dial rpc again in %s", retryInterval.String())}
		}
		<-time.After(retryInterval)
	}
	opts.Events <- NewClientEvent{Message: fmt.Sprintf("successfully connected to rpc[%s]", rpcUrl)}

	// get chain id
	chainIdMaxRetries := opts.MaxRetries
	if chainIdMaxRetries <= 0 {
		chainIdMaxRetries = 1
	}

	var chainIdError error

	opts.Events <- NewClientEvent{
		Message: fmt.Sprintf("getting chain id of rpc[%s]...", rpcUrl),
	}

	var chainId *big.Int = nil
	for chainId == nil {
		chainId, chainIdError = clientInstance.ChainID(context.Background())
		if chainIdError != nil {
			opts.Events <- NewClientEvent{Error: fmt.Errorf("failed to get chain id from rpc[%s]: %s", rpcUrl, chainIdError)}
			chainIdMaxRetries--
			if chainIdMaxRetries == 0 {
				return clientInstance, nil, fmt.Errorf("failed to get chain id from rpc[%s] after %v tries: %s", rpcUrl, opts.MaxRetries, chainIdError)
			}
			opts.Events <- NewClientEvent{Message: fmt.Sprintf("trying to get chain id again in %s", retryInterval.String())}
		}
		<-time.After(retryInterval)
	}
	opts.Events <- NewClientEvent{Message: fmt.Sprintf("successfully received id for chain[%v]", chainId)}
	return clientInstance, chainId, nil
}
