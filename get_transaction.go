package evm

import (
	"context"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

type GetTransactionOpts struct {
	Client          *ethclient.Client
	ChainId         *big.Int
	IncludeBlock    bool
	IncludeLogs     bool
	TransactionHash common.Hash
}

func GetTransaction(opts GetTransactionOpts) (*Transaction, error) {
	if opts.ChainId == nil {
		var err error
		opts.ChainId, err = opts.Client.ChainID(context.Background())
		if err != nil {
			return nil, fmt.Errorf("failed to get chain id: %s", err)
		}
	}
	transaction, _, err := opts.Client.TransactionByHash(context.Background(), opts.TransactionHash)
	if err != nil {
		return nil, fmt.Errorf("failed to get transaction[%s]: %s", opts.TransactionHash, err)
	}
	message, err := core.TransactionToMessage(transaction, types.NewLondonSigner(opts.ChainId), nil)
	if err != nil {
		return nil, fmt.Errorf("failed to get transaction[%s] as message: %s", transaction.Hash().Hex(), err)
	}
	var receipt *types.Receipt = nil
	if opts.IncludeLogs {
		var receiptErr error
		receipt, receiptErr = opts.Client.TransactionReceipt(context.Background(), transaction.Hash())
		if receiptErr != nil {
			return nil, fmt.Errorf("failed to get receipt of transaction[%s]: %s", transaction.Hash().Hex(), receiptErr)
		}
	}
	var block *types.Block = nil
	if opts.IncludeBlock {
		var blockErr error
		block, blockErr = opts.Client.BlockByNumber(context.Background(), receipt.BlockNumber)
		if blockErr != nil {
			return nil, fmt.Errorf("failed to get block[%v] for transaction[%s]: %s", receipt.BlockNumber.Int64(), transaction.Hash().Hex(), blockErr)
		}
	}
	transactionInstance := NewTransaction(transaction, message, receipt, block)
	return transactionInstance, nil
}
