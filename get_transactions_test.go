package evm

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestGetTransactions(t *testing.T) {
	suite.Run(t, &GetTransactionsTests{})
}

type GetTransactionsTests struct {
	suite.Suite
}

func (t ClientTests) Test_GetTransactions() {
	client, _, err := NewClient(NewClientOpts{RpcUrl: "https://evm.cronos.org"})
	t.Nil(err)
	currentBlockNumber, err := client.BlockNumber(context.TODO())
	t.Nil(err)
	expectedBlockNumber := int64(currentBlockNumber) - 1
	transactionsCount := 0

	var transactions Transactions
	for transactionsCount == 0 {
		transactions, err = GetTransactionsFromBlock(GetTransactionsFromBlockOpts{
			Client:      client,
			BlockNumber: expectedBlockNumber,
		})
		t.Nil(err)
		transactionsCount = len(transactions)
	}
	targetTransaction := transactions[0]
	t.NotZero(targetTransaction.BlockNumber)
	t.NotEmpty(targetTransaction.Hash)
	t.NotEmpty(targetTransaction.From)
	t.NotEmpty(targetTransaction.To)
	if targetTransaction.Cost.Int64() != 0 {
		t.NotZero(targetTransaction.Value.Int64())
	}
}
