package evm

import (
	"context"
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

type GetTransactionsFromBlockOpts struct {
	Client      *ethclient.Client
	ChainId     *big.Int
	BlockNumber int64
	IncludeLogs bool
}

func GetTransactionsFromBlock(opts GetTransactionsFromBlockOpts) (Transactions, error) {
	if opts.ChainId == nil {
		var err error
		opts.ChainId, err = opts.Client.ChainID(context.Background())
		if err != nil {
			return nil, fmt.Errorf("failed to get chain id: %s", err)
		}
	}
	block, err := opts.Client.BlockByNumber(context.Background(), big.NewInt(opts.BlockNumber))
	if err != nil {
		return nil, fmt.Errorf("failed to get block[%v]: %s", opts.BlockNumber, err)
	}
	transactionsInstance := Transactions{}
	transactions := block.Transactions()
	for _, transaction := range transactions {
		message, err := core.TransactionToMessage(transaction, types.NewLondonSigner(opts.ChainId), nil)
		if err != nil {
			return nil, fmt.Errorf("failed to get transaction[%s] as message: %s", transaction.Hash().Hex(), err)
		}
		var receipt *types.Receipt = nil
		if opts.IncludeLogs {
			var receiptErr error
			receipt, receiptErr = opts.Client.TransactionReceipt(context.Background(), transaction.Hash())
			if receiptErr != nil {
				return nil, fmt.Errorf("failed to get receipt of transaction[%s]: %s", transaction.Hash().Hex(), receiptErr)
			}
		}
		transactionInstance := NewTransaction(transaction, message, receipt, block)
		transactionsInstance = append(transactionsInstance, *transactionInstance)
	}
	return transactionsInstance, nil
}
