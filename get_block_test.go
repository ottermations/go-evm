package evm

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestGetBlock(t *testing.T) {
	suite.Run(t, &GetBlockTests{})
}

type GetBlockTests struct {
	suite.Suite
}

func (t ClientTests) Test_GetBlock() {
	client, _, err := NewClient(NewClientOpts{RpcUrl: "https://evm.cronos.org"})
	t.Nil(err)
	currentBlockNumber, err := client.BlockNumber(context.TODO())
	t.Nil(err)
	expectedBlockNumber := int64(currentBlockNumber) - 1
	block, err := GetBlock(GetBlockOpts{
		Client: client,
		Number: expectedBlockNumber,
	})
	t.Nil(err)
	t.EqualValues(expectedBlockNumber, block.Number)
}

func (t ClientTests) Test_GetBlock_PolygonEdgeCase() {
	client, _, err := NewClient(NewClientOpts{RpcUrl: "https://polygon-rpc.com"})
	t.Nil(err)
	expectedBlockNumber := int64(41821456)
	block, err := GetBlock(GetBlockOpts{
		Client: client,
		Number: expectedBlockNumber,
	})
	t.Nil(err)
	t.EqualValues(expectedBlockNumber, block.Number)
	t.NotEmpty(block.Hash)
	t.NotNil(block.TransactionHashes)
	t.Empty(block.TransactionHashes)
}
