package evm

import (
	"context"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/suite"
)

func TestGetTransaction(t *testing.T) {
	suite.Run(t, &GetTransactionTests{})
}

type GetTransactionTests struct {
	suite.Suite
}

func (t ClientTests) Test_GetTransaction() {
	client, _, err := NewClient(NewClientOpts{RpcUrl: "https://evm.cronos.org"})
	t.Nil(err)
	currentBlockNumber, err := client.BlockNumber(context.TODO())
	t.Nil(err)
	expectedBlockNumber := int64(currentBlockNumber) - 1
	transactionsCount := 0

	var block *Block
	for transactionsCount == 0 {
		block, err = GetBlock(GetBlockOpts{
			Client: client,
			Number: expectedBlockNumber,
		})
		t.Nil(err)
		t.EqualValues(expectedBlockNumber, block.Number)
		transactionsCount = block.TransactionsCount
		expectedBlockNumber--
	}

	transaction, err := GetTransaction(GetTransactionOpts{
		Client:          client,
		TransactionHash: common.HexToHash(block.TransactionHashes[0]),
	})
	t.Nil(err)
	t.NotNil(transaction)
	t.Nil(transaction.Logs)
	t.Zero(transaction.BlockNumber)

	transaction, err = GetTransaction(GetTransactionOpts{
		Client:          client,
		TransactionHash: common.HexToHash(block.TransactionHashes[0]),
		IncludeLogs:     true,
	})
	t.Nil(err)
	t.NotNil(transaction.Logs)

	transaction, err = GetTransaction(GetTransactionOpts{
		Client:          client,
		TransactionHash: common.HexToHash(block.TransactionHashes[0]),
		IncludeBlock:    true,
	})
	t.Nil(err)
	t.NotZero(transaction.BlockNumber)

}
