package evm

const (
	ChainArbitrum    = "arbitrum"
	ChainAptos       = "aptos"
	ChainAvalanche   = "avalanche"
	ChainBinance     = "bsc"
	ChainBinanceId   = 56
	ChainCronos      = "cronos"
	ChainCronosId    = 25
	ChainEthereum    = "ethereum"
	ChainEthereumId  = 1
	ChainFantom      = "fantom"
	ChainFantomId    = 250
	ChainMoonbeam    = "moonbeam"
	ChainOptimism    = "optimism"
	ChainPolygon     = "polygon"
	ChainPolygonId   = 137
	ChainPolygonZk   = "polygonzk"
	ChainPolygonZkId = 1101
	ChainSolana      = "solana"
)
