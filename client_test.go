package evm

import (
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestClient(t *testing.T) {
	suite.Run(t, &ClientTests{})
}

type ClientTests struct {
	suite.Suite
}

func (t ClientTests) Test_NewClient() {
	client, chainId, err := NewClient(NewClientOpts{RpcUrl: "https://evm.cronos.org"})
	t.Nil(err)
	t.EqualValues(25, chainId.Int64())
	t.NotNil(client)
}

func (t ClientTests) Test_NewClient_WithEvents() {
	allEvents := []NewClientEvent{}
	events := make(chan NewClientEvent)
	var waiter sync.WaitGroup
	waiter.Add(1)
	go func() {
		for {
			event, valid := <-events
			if !valid {
				waiter.Done()
				return
			}
			allEvents = append(allEvents, event)
		}
	}()
	client, chainId, err := NewClient(NewClientOpts{
		RpcUrl: "https://evm.cronos.org",
		Events: events,
	})
	t.Nil(err)
	t.EqualValues(25, chainId.Int64())
	t.NotNil(client)
	close(events)
	waiter.Wait()
	for _, event := range allEvents {
		fmt.Println(event.Message)
	}
	fmt.Println("HI")
}
