package evm

import (
	"context"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/suite"
)

func TestGetLogs(t *testing.T) {
	suite.Run(t, &GetLogsTests{})
}

type GetLogsTests struct {
	suite.Suite
}

func (t ClientTests) Test_GetLogs() {
	client, _, err := NewClient(NewClientOpts{RpcUrl: "https://evm.cronos.org"})
	t.Nil(err)
	currentBlockNumber, err := client.BlockNumber(context.TODO())
	t.Nil(err)
	expectedBlockNumber := int64(currentBlockNumber) - 1
	transactionsCount := 0

	var block *Block
	for transactionsCount == 0 {
		block, err = GetBlock(GetBlockOpts{
			Client: client,
			Number: expectedBlockNumber,
		})
		t.Nil(err)
		t.EqualValues(expectedBlockNumber, block.Number)
		if block.TransactionsCount == 0 {
			expectedBlockNumber--
			continue
		}

		logsFound := false
		for i := 0; i < block.TransactionsCount; i++ {
			expectedTransactionHash := common.HexToHash(block.TransactionHashes[i])
			logs, err := GetLogs(GetLogsOpts{
				Client:          client,
				TransactionHash: expectedTransactionHash,
			})
			t.Nil(err)
			t.NotNil(logs)
			if len(logs) > 0 {
				logsFound = true
				t.NotEmpty(logs[0].Address)
				t.NotEmpty(logs[0].BlockNumber)
				break
			}
		}

		if !logsFound {
			expectedBlockNumber--
			continue
		}
	}

}
