package evm

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
)

type GetBlockOpts struct {
	Client *ethclient.Client
	Number int64
}

func GetBlock(opts GetBlockOpts) (*Block, error) {
	if opts.Number == 0 {
		blockNumber, err := opts.Client.BlockNumber(context.Background())
		if err != nil {
			return nil, fmt.Errorf("failed to get latest block number: %s", err)
		}
		opts.Number = int64(blockNumber)
	}
	block, err := opts.Client.BlockByNumber(context.Background(), big.NewInt(opts.Number))
	if err != nil {
		reportedChainId := int64(0)
		chainId, _ := opts.Client.ChainID(context.TODO())
		if chainId != nil {
			reportedChainId = chainId.Int64()
		}
		// edge case for polygon where some blocks are invalid
		if strings.Contains(err.Error(), "server returned non-empty transaction list but block header indicates no transactions") {
			nextBlockNumber := big.NewInt(0).Add(big.NewInt(opts.Number), big.NewInt(1))
			nextBlock, err := opts.Client.BlockByNumber(context.Background(), nextBlockNumber)
			if err != nil {
				return nil, fmt.Errorf("failed to get block[%v] from chain[%v]: %s", opts.Number, reportedChainId, err)
			}
			previousBlockNumber := big.NewInt(0).Add(big.NewInt(opts.Number), big.NewInt(-1))
			previousBlock, err := opts.Client.BlockByNumber(context.Background(), previousBlockNumber)
			if err != nil {
				return nil, fmt.Errorf("failed to get block[%v] from chain[%v]: %s", opts.Number, reportedChainId, err)
			}
			blockTimestamp := time.Unix(int64(previousBlock.Time()+(nextBlock.Time()-previousBlock.Time())), 0)
			return &Block{
				Hash:              nextBlock.ParentHash().Hex(),
				ParentHash:        previousBlock.Hash().Hex(),
				Number:            opts.Number,
				Timestamp:         blockTimestamp,
				TransactionHashes: []string{},
			}, nil
		} else {
			return nil, fmt.Errorf("failed to get block[%v] from chain[%v]: %s", opts.Number, reportedChainId, err)
		}
	}
	blockInstance := NewBlock(block)
	return blockInstance, nil
}

// 41821456 - 0xdd13b2d8c15f17c70a7351201edb1d2209d0ebd0315ab66e1715cb1fac2cd094
// parent   - 0xdd13b2d8c15f17c70a7351201edb1d2209d0ebd0315ab66e1715cb1fac2cd094
