package evm

import (
	"math/big"
	"time"
)

type Block struct {
	BaseFee           *big.Int  `json:"baseFee"`
	GasLimit          uint64    `json:"gasLimit"`
	GasUsed           uint64    `json:"gasUsed"`
	Hash              string    `json:"hash"`
	Number            int64     `json:"number"`
	ParentHash        string    `json:"parentHash"`
	Timestamp         time.Time `json:"timestamp"`
	TransactionHashes []string  `json:"transactionHashes"`
	TransactionsCount int       `json:"transactionsCount"`
	UncleHash         string    `json:"uncleHash"`
	ValidatedBy       string    `json:"validatedBy"`
}

type Transactions []Transaction

type Transaction struct {
	BlockNumber int64     `json:"blockNumber"`
	Cost        *big.Int  `json:"cost"`
	Data        string    `json:"data"`
	From        string    `json:"from"`
	GasLimit    uint64    `json:"gasLimit"`
	Hash        string    `json:"hash"`
	Index       uint      `json:"index"`
	Logs        Logs      `json:"logs"`
	Method      string    `json:"method"`
	Timestamp   time.Time `json:"timestamp"`
	To          string    `json:"to"`
	Value       *big.Int  `json:"value"`
}

type Logs []Log

type Log struct {
	Address         string   `json:"address"`
	BlockNumber     uint64   `json:"blockNumber"`
	Data            string   `json:"data"`
	Index           uint     `json:"index"`
	Topics          []string `json:"topics"`
	TransactionHash string   `json:"transactionHash,omitempty"`
}
